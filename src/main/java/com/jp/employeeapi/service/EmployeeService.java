package com.jp.employeeapi.service;

import com.jp.employeeapi.domain.Employee;
import com.jp.employeeapi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

//    Get/View All
    public List<Employee> viewAllEmployees(){
        return employeeRepository.findAll();
    }


//    Get Employee by Supervisor ID
    public List<Employee> getEmployeesBySupervisorId(Long id){
        return employeeRepository.getEmployeesBySupervisorId(id);
    }

//    Get/View Single
    public Optional<Employee> viewEmployeeById(Long id){
        return employeeRepository.findById(id);

    }


//    Add
    public Employee addEmployee(Employee employee){
        return employeeRepository.save(employee);
    }

//    Edit
    public Employee editEmployee(Long id, Employee employee){
        employee.setId(id);
        return employeeRepository.save(employee);
    }

//    Delete
    public void deleteEmployee(Long id){
        employeeRepository.deleteById(id);
    }
}
