package com.jp.employeeapi.service;

import com.jp.employeeapi.domain.Employee;
import com.jp.employeeapi.domain.Supervisor;
import com.jp.employeeapi.repository.EmployeeRepository;
import com.jp.employeeapi.repository.SupervisorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SupervisorService {

    private SupervisorRepository supervisorRepository;
    private EmployeeRepository employeeRepository;

    @Autowired
    public SupervisorService(SupervisorRepository supervisorRepository, EmployeeRepository employeeRepository) {
        this.supervisorRepository = supervisorRepository;
        this.employeeRepository = employeeRepository;
    }

//    Get/View
    public List<Supervisor> viewAllSupervisors(){
        return supervisorRepository.findAll();
    }

//    Get/View Single
    public Optional<Supervisor> viewSupervisorById(Long id){
    return supervisorRepository.findById(id);

}

//    Add
    public Supervisor addSupervisor(Supervisor supervisor){
        return supervisorRepository.save(supervisor);
    }

//    Edit
    public Supervisor editSupervisor(Long id, Supervisor supervisor){
        supervisor.setId(id);
        return supervisorRepository.save(supervisor);
    }

//    Delete
    public void deleteSupervisor(Long id){
    supervisorRepository.deleteById(id);
}

}
