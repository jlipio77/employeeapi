package com.jp.employeeapi.controller;

import com.jp.employeeapi.domain.Employee;
import com.jp.employeeapi.domain.Supervisor;
import com.jp.employeeapi.service.EmployeeService;
import com.jp.employeeapi.service.SupervisorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("supervisors")
@CrossOrigin(origins = "*")
public class SupervisorController {

    private SupervisorService supervisorService;
    private EmployeeService employeeService;

    @Autowired
    public SupervisorController(SupervisorService supervisorService, EmployeeService employeeService) {
        this.supervisorService = supervisorService;
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<Supervisor> viewAllSupervisors(){
        return supervisorService.viewAllSupervisors();
    }

    @GetMapping("view")
    public Optional<Supervisor> viewSupervisorById(Long id) {
        return supervisorService.viewSupervisorById(id);
    }

    @PostMapping
    public Supervisor addSupervisor(@RequestBody Supervisor supervisor){
        return supervisorService.addSupervisor(supervisor);
    }

    @PutMapping("edit")
    public Supervisor editSupervisor(@RequestParam Long id, @RequestBody Supervisor supervisor){
        return supervisorService.editSupervisor(id, supervisor);
    }

    @DeleteMapping("delete")
    public void deleteSupervisor(@RequestParam Long id){
        supervisorService.deleteSupervisor(id);
    }


}
