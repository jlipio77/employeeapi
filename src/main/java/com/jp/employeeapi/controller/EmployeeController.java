package com.jp.employeeapi.controller;

import com.jp.employeeapi.domain.Employee;
import com.jp.employeeapi.service.EmployeeService;
import com.jp.employeeapi.service.SupervisorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("employees")
@CrossOrigin(origins = "*")
public class EmployeeController {

    private EmployeeService employeeService;
    private SupervisorService supervisorService;

    @Autowired
    public EmployeeController(EmployeeService employeeService, SupervisorService supervisorService) {
        this.employeeService = employeeService;
        this.supervisorService = supervisorService;
    }

    @GetMapping
    public List<Employee> viewAllEmployees(){
        return employeeService.viewAllEmployees();
    }

    @GetMapping("view")
    public Optional<Employee> viewEmployeeById(@RequestParam Long id) {
        return employeeService.viewEmployeeById(id);
    }

    @GetMapping("list")
    public List<Employee> getEmployeesBySupervisorId(@RequestParam Long id){
        return employeeService.getEmployeesBySupervisorId(id);
    }

    @PostMapping
    public Employee addEmployee(@RequestBody Employee employee){
        return employeeService.addEmployee(employee);
    }

    @PutMapping("edit")
    public Employee editEmployee(@RequestParam Long id, @RequestBody Employee employee){
        return employeeService.editEmployee(id, employee);
    }

    @DeleteMapping("delete")
    public void deleteEmployee(@RequestParam Long id){
        employeeService.deleteEmployee(id);
    }
}
