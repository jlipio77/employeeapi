package com.jp.employeeapi.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Supervisor {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    public Long id;

    public String firstName;
    public String lastName;
    public String gender;

    @OneToMany(mappedBy = "supervisor")
    private List<Employee> employeeList;

    public Supervisor(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public Supervisor() {
    }

    public Supervisor(Long id, String firstName, String lastName, String gender, List<Employee> employeeList) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.employeeList = employeeList;
    }

    public Supervisor(Long id, String firstName, String lastName, String gender) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Supervisor that = (Supervisor) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Supervisor{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", employeeList=" + employeeList +
                '}';
    }
}
